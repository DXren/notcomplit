﻿namespace Labir
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.Labirint = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.button1 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Labirint
            // 
            this.Labirint.Location = new System.Drawing.Point(74, 484);
            this.Labirint.Name = "Labirint";
            this.Labirint.Size = new System.Drawing.Size(282, 50);
            this.Labirint.TabIndex = 0;
            this.Labirint.Text = "Labirint";
            this.Labirint.UseVisualStyleBackColor = true;
            this.Labirint.Click += new System.EventHandler(this.Labirint_Click);
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Location = new System.Drawing.Point(676, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(665, 552);
            this.panel1.TabIndex = 0;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint_1);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(74, 537);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(282, 57);
            this.button1.TabIndex = 2;
            this.button1.Text = "Escape";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = global::Labir.Properties.Resources.a;
            this.ClientSize = new System.Drawing.Size(1375, 606);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.Labirint);
            this.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.Name = "Form1";
            this.Text = "Царь Минас привествует вас!";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button Labirint;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button button1;
    }
}

