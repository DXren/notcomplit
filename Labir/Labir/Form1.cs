﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Labir
{
    public enum Lab
    {
        Wall,
        Cell,
        Xperson,
        Yperson
    }
    public partial class Form1 : Form
    {
        int raz = 10;//размер базовой ячейки, в пикселях
        Lab[,] Map;
        int height = 201;// ширина лабиринта, в ячейках
        int width = 201;
        Point currentCell, StartCell, FinishCell;
        Stack<Point> stack;
        Random rd;
        bool risyn = false;
        private void InitMap()
        {
            for (var i = 0; i < width; i++)
            {
                for (var j = 0; j < height; j++)
                {
                    if (i % 2 != 0 && j % 2 != 0 &&         
                        i < width - 1 && j < height - 1)    
                        Map[i, j] = Lab.Cell;             // тут клетка
                    else Map[i, j] = Lab.Wall;            // стена
                }
            }
        }
        public Form1()
        {
            InitializeComponent();
            panel1.Size = new Size(width * raz, height * raz);
            stack = new Stack<Point>();
            rd = new Random();
            Map = new Lab[width, height];
            InitMap();
            currentCell = new Point(1, 1);
        }
        public void DrawBox(int x, int y, Brush b, Graphics gr)
        {
            gr.FillRectangle(b, x * raz, y * raz, raz, raz);
        }
        

        private Point[] GetSosedi(Point c)
        {
            const int distance = 2;
            var points = new List<Point>();
            var x = c.X;
            var y = c.Y;
            var up = new Point(x, y - distance);
            var rt = new Point(x + distance, y);
            var dw = new Point(x, y + distance);
            var lt = new Point(x - distance, y);
            var d = new Point[] { dw, rt, up, lt };
            foreach (var p in d)
            {
                if (p.X > 0 && p.X < width && p.Y > 0 && p.Y < height)
                {
                    if (Map[p.X, p.Y] != Lab.Wall && Map[p.X, p.Y] != Lab.Xperson)
                        points.Add(p);
                }
            }
            return points.ToArray();
        }
        private void RemoveWall(Point first, Point second)
        {
            var xDiff = second.X - first.X;
            var yDiff = second.Y - first.Y;
            int addX, addY;
            var target = new Point();

            addX = (xDiff != 0) ? (xDiff / Math.Abs(xDiff)) : 0;
            addY = (yDiff != 0) ? (yDiff / Math.Abs(yDiff)) : 0;

            target.X = first.X + addX;
            target.Y = first.Y + addY;

            Map[target.X, target.Y] = Lab.Xperson;
        }
        
        private void Labirint_Click(object sender, EventArgs e)
        {
            if (risyn)
            {
                InitMap();
                currentCell = new Point(1, 1);
            }
            BuildMap();
            PrepareAfterBuildMap();
            risyn= true;
            panel1.Invalidate();
        }

        private void panel1_Paint_1(object sender, PaintEventArgs e)
        {
            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    switch (Map[i, j])
                    {
                        case Lab.Cell:
                            DrawBox(i, j, Brushes.White, e.Graphics);
                            break;
                        case Lab.Wall:
                            DrawBox(i, j, Brushes.Black, e.Graphics);
                            break;
                        case Lab.Xperson:
                            DrawBox(i, j, Brushes.Blue, e.Graphics);
                            break;
                        case Lab.Yperson:
                            DrawBox(i, j, Brushes.Red, e.Graphics);
                            break;
                    }
                }
            }
        }

        private void BuildMap()
        {
            while (true)
            {
                var sosedi = GetSosedi(currentCell);
                if (sosedi.Length != 0)
                {
                    var randNum = rd.Next(sosedi.Length);
                    var SosedCell = sosedi[randNum];
                    if (sosedi.Length > 1) stack.Push(currentCell);
                    RemoveWall(currentCell, SosedCell);
                    Map[currentCell.X, currentCell.Y] = Lab.Xperson;
                    currentCell = SosedCell;
                }
                else if (stack.Count > 0)
                {
                    Map[currentCell.X, currentCell.Y] = Lab.Xperson;
                    currentCell = stack.Pop();
                }
                else break;
            }
        }
        private void PrepareAfterBuildMap()
        {
            for (var i = 0; i < width; i++)
            {
                for (var j = 0; j < height; j++)
                {
                    if (Map[i, j] == Lab.Xperson)
                        Map[i, j] = Lab.Cell;
                }
            }
        }
    }
    

}
